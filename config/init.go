package config

import (
	"flag"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// 配置文件对应参数结构
type ConfType struct {
	App      string   `json:"app,omitempty"`      // 应用名称
	Port     string   `json:"port,omitempty"`     // 服务端口
	Mode     string   `json:"mode,omitempty"`     // 模式
	Timeout  uint     `json:"timeout,omitempty"`  // 超时时间，获取监听对象情报的超时时间
	Duration uint     `json:"duration,omitempty"` // 间隔时间，获取监听对象情报的间隔时间
	Retry    uint     `json:"retry,omitempty"`    // 重试次数，获取监听对象情报失败后的重试次数
	Postfix  string   `json:"postfix,omitempty"`  // 监听对象的路由后缀，默认为 '/metrics'
	Target   []string `json:"target,omitempty"`   // 监听对象的路由, host:port
}

var Conf *ConfType
var DebugMode bool

func init() {

	// 根据gin启动/编译的模式读取不同的配置文件
	flag.BoolVar(&DebugMode, "debug", false, "Debug mode")
	flag.Parse()

	log.Printf("[DebugMode] %v\n", DebugMode)

	if DebugMode {
		gin.SetMode(gin.DebugMode)
		viper.SetConfigName("config.dev")
	} else {
		gin.SetMode(gin.ReleaseMode)
		viper.SetConfigName("config.prod")
	}
	// 配置文件后缀
	viper.SetConfigType("yaml")
	// 配置文件查找目录
	viper.AddConfigPath(".")

	// 读取文件
	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Panicln("config file not found")
		} else {
			log.Panicf("config file read failed: %v\n", err)
		}
	}

	// 绑定结构体
	if err := viper.Unmarshal(&Conf); err != nil {
		log.Panicf("config file convert failed: %v\n", err)
	}

	log.Println("initialize config end")
}
