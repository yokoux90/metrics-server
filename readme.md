# build in debug mode
The `config.dev.yaml` file will be loaded.

`go build --debug`

# build in release mode
The `config.prod.yaml` file will be loaded.

`go build`