#!/usr/bin/bash

set GOOS=linux
set GOARCH=amd64

if [ -d "./dist" ]; then
    rm -rf ./dist
    mkdir ./dist
else    
    mkdir ./dist
fi

go build -o ./dist/serve
cp -R ./static ./dist
cp -R ./templates ./dist
cp config.prod.yaml ./dist