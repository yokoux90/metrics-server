package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"text/template"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/recika/middleware/recika-monitor-server/config"
	"gitlab.com/recika/middleware/recika-monitor-server/handler"
)

func init() {
	log.SetPrefix("[INFO] ")
}

func main() {

	log.Printf("config: %v\n", config.Conf)

	r := gin.Default()

	// 设置静态资源目录
	r.Static("/static", "./static")

	// 注册模板函数
	r.SetFuncMap(template.FuncMap{
		"toMB": func(v uint64) string {
			result := v / (1024 * 1024)
			return fmt.Sprintf("%vMB", result)
		},
		"toGB": func(v uint64) string {
			result := v / (1024 * 1024 * 1024)
			return fmt.Sprintf("%vGB", result)
		},
		"toPercent": func(v float64) string {
			return fmt.Sprintf("%.2f%%", v)
		},
		"toTime": func(v int64) string {
			t := time.Unix(v, 0)
			return t.Format("15:04:05")
		},
	})

	// 加载模板资源
	r.LoadHTMLGlob("templates/**/*")

	// 路由配置
	r.GET("/", handler.IndexGetHandler)
	r.GET("/realtime", handler.RealtimeGetHandler)

	r.GET("/detail", handler.DetailGetHandler)

	// WebSocket路由设置
	r.GET("/ws", handler.WebSocketHandler)

	// 新建服务
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", config.Conf.Port),
		Handler: r,
	}
	log.Printf("Running on http://localhost:%s\n", fmt.Sprintf(":%s", config.Conf.Port))

	// 启动监听
	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	// 监听系统信号
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Panicln("Shutting down server...")

	// 配置上下文超时
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")
}
