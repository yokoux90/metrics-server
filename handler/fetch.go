package handler

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	metrics "gitlab.com/recika/middleware/recika-monitor-client"
	"gitlab.com/recika/middleware/recika-monitor-server/config"
)

// 获取监听服务情报
func Fetch(target_url string) (result *metrics.MetricsResponse, err error) {
	client := &http.Client{Timeout: time.Duration(config.Conf.Timeout) * time.Second}
	defer client.CloseIdleConnections()
	resp, err := client.Get(target_url)

	if err != nil {
		log.Printf("request %v failed: %v\n", target_url, err)
		return nil, err
	}

	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("Cann't parse infomation from node.")
		return nil, err
	}
	err = json.Unmarshal(content, &result)
	if err != nil {
		log.Println("Cann't convert response content.")
		return nil, err
	}
	return result, nil
}
