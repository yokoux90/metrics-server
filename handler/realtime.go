package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/recika/middleware/recika-monitor-server/config"
)

// 获取实时路由情报
// 渲染 realtime.html 模板
func RealtimeGetHandler(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "realtime.html", gin.H{
		"target":  nil,
		"targets": config.Conf.Target,
		"info":    nil,
		"online":  false,
	})
}
