package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/recika/middleware/recika-monitor-server/config"
)

// 服务状态
const (
	ONLINE      uint   = 0 // 在线
	RETRY       uint   = 1 // 重试中
	OFFLINE     uint   = 2 // 离线
	TIME_FORMAT string = "2006-01-02 15:04:05"
)

// Socket返回的消息
type IMessage struct {
	Target  string      `json:"target"`  // 目标服务地址，在配置文件中配置
	Retry   uint        `json:"retry"`   // 重试次数
	Status  uint        `json:"status"`  // 服务状态
	Data    interface{} `json:"data"`    // 服务数据
	Updated string      `json:"updated"` // 更新状态情报时间
}

// Socket连接
type WSock struct {
	// Socket通道加锁，同时只允许一个协程发送消息
	sync.Mutex
	Conn *websocket.Conn
}

// 封装Socket发送JSON消息，加互斥锁
func (ws *WSock) WriteJSON(v interface{}) {
	ws.Lock()
	ws.Conn.WriteJSON(v)
	ws.Unlock()
}

// 封装Socket发送Text消息，加互斥锁
func (ws *WSock) WriteMessage(v interface{}) {
	ws.Lock()
	data, err := json.Marshal(v)
	if err != nil {
		log.Println("[Error] JSON conver failed")
	}
	ws.Conn.WriteMessage(websocket.TextMessage, data)
	ws.Unlock()
}

// 定时处理
func tickerHandler(ws *WSock, dt time.Time) {
	log.Printf("[%s] start gorutine", dt.Format(TIME_FORMAT))
	// 循环检测目标服务器
	for _, url := range config.Conf.Target {
		// 开启协程检测心跳
		go func(target_url string, trigger_time time.Time) {
			log.Printf("[%s] check %s", trigger_time.Format(TIME_FORMAT), target_url)
			// 重试次数计数
			var retry_count uint = 0
			for retry_count <= config.Conf.Retry {
				result, err := Fetch(target_url + config.Conf.Postfix)
				if err != nil {
					// 失败则重试
					if retry_count >= config.Conf.Retry {
						// 超过重试次数则下线
						ws.WriteJSON(&IMessage{
							Target:  target_url,
							Retry:   retry_count,
							Status:  OFFLINE,
							Data:    nil,
							Updated: trigger_time.Format(TIME_FORMAT),
						})
						break
					} else {
						log.Printf("[%s] fetch %s, retry %d times", trigger_time.Format(TIME_FORMAT), target_url, retry_count)
						// 未超过重试次数则休眠3秒后继续重试
						ws.WriteJSON(&IMessage{
							Target:  target_url,
							Retry:   retry_count,
							Status:  RETRY,
							Data:    nil,
							Updated: trigger_time.Format(TIME_FORMAT),
						})
						time.Sleep(3 * time.Second)
						retry_count += 1
					}
				} else {
					log.Printf("[%s] ok\n", trigger_time.Format(TIME_FORMAT))
					ws.WriteMessage(&IMessage{
						Target:  target_url,
						Retry:   retry_count,
						Status:  ONLINE,
						Data:    result,
						Updated: trigger_time.Format(TIME_FORMAT),
					})
					break
				}
			}
		}(url, dt)
	}
}

// Socket处理函数
// 定时检测监听服务
func WebSocketHandler(ctx *gin.Context) {
	ws := &websocket.Upgrader{
		HandshakeTimeout: 30 * time.Second,
		WriteBufferSize:  1024 * 1024,
		CheckOrigin:      func(r *http.Request) bool { return true },
	}
	conn, err := ws.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		log.Panicf("Web socket initialize failed: %v\n", err)
	}
	defer conn.Close()

	w_sock := &WSock{
		Conn: conn,
	}

	t := time.NewTicker(time.Duration(config.Conf.Duration) * time.Second)

	// 连接后首次触发
	tickerHandler(w_sock, time.Now())
	// 计时器协程
	go func() {
		for dt := range t.C {
			// 定时器触发
			tickerHandler(w_sock, dt)
		}
	}()
	defer func() {
		t.Stop()
		log.Println("Tikcer stopped")
	}()

	// 循环等待
	for {
		messageType, p, err := w_sock.Conn.ReadMessage()
		if err != nil {
			t.Stop()
			log.Println("Tikcer stopped")
			break
		}

		log.Println("messageType: ", messageType)
		log.Println("p: ", string(p))

		// 回显测试
		// ctx.Writer.Write(p)

		// 其他操作
		// todo
	}
}
