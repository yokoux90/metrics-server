package handler

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	metrics "gitlab.com/recika/middleware/recika-monitor-client"
	"gitlab.com/recika/middleware/recika-monitor-server/config"
)

// 获取监听对象详情处理函数
// 渲染 detail.html 模板
func DetailGetHandler(ctx *gin.Context) {
	// 获取请求参数：监听对象路由
	params := make(map[string]string)
	err := ctx.ShouldBind(&params)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Invalid parameters",
		})
	}

	target := params["target"]

	// 组装完整路由： 路由 + 后缀
	target_url := target + config.Conf.Postfix
	log.Printf("request: %v\n", target_url)

	// 创建http请求
	var result *metrics.MetricsResponse
	// 设置请求超时
	client := &http.Client{Timeout: time.Duration(config.Conf.Timeout) * time.Second}
	resp, err := client.Get(target_url)
	if err != nil {
		log.Printf("request %v failed: %v\n", target_url, err)
		ctx.HTML(http.StatusBadRequest, "detail.html", gin.H{
			"target": target,
			"info":   nil,
			"online": false,
		})
		return
	}

	// 读取返回
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		ctx.JSON(http.StatusNotAcceptable, gin.H{
			"message": "Cann't parse infomation from node.",
		})
	}
	// 绑定返回数据到结构体
	err = json.Unmarshal(content, &result)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Cann't convert response content.",
		})
	}
	// 渲染模板
	ctx.HTML(http.StatusOK, "detail.html", gin.H{
		"target": target,
		"info":   result,
		"online": true,
	})
}
