package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/recika/middleware/recika-monitor-server/config"
)

// 首页路由处理函数
// 渲染 index.html 模板
func IndexGetHandler(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "index.html", gin.H{
		"target":  nil,
		"targets": config.Conf.Target,
		"info":    nil,
		"online":  false,
	})
}
